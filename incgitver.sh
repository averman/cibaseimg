#!/bin/bash
git describe --tags >> /dev/null
if [ $? == 128 ]; then
  echo 0.0.1
else
  GITVER=$(git describe --abbrev=0 --tags)
  BUILDVER=${GITVER//*.*./}
  MAJORMINORVER=${GITVER:0:$((${#GITVER}-${#BUILDVER}-1))}
  GITMINOR=${MAJORMINORVER//*./}
  GITMAJOR=${MAJORMINORVER//.*/}
  BUILDVER=$((BUILDVER+1))
  echo $GITMAJOR.$GITMINOR.$BUILDVER
fi