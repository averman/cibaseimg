FROM docker:latest
COPY *.sh /usr/local/bin/
RUN apk update && apk upgrade \
    && apk add --no-cache bash git openssh make py-pip sudo wget \
    && pip install --upgrade pip \
    && pip install docker-compose \
    && chmod 777 /usr/local/bin/*.sh
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["bash"]