#!/bin/bash
read GITVER
docker login -u "gitlab-ci-token" -p "$CI_JOB_TOKEN" $CI_REGISTRY
docker build --pull -t "$CI_REGISTRY_IMAGE:latest" -t "$CI_REGISTRY_IMAGE:$GITVER" .
docker push "$CI_REGISTRY_IMAGE:$GITVER"
docker push "$CI_REGISTRY_IMAGE:latest"
exit 0