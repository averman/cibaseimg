# `cibaseimg` docker image

This image is meant to be used for base docker image for runner in `.gitlab-ci.yml` 
available at `registry2.dattabot.io/gilang/cibaseimg:<version>` for public use.
The purpose for this image is to provide you base image for the gitlab-ci-runner that contains docker 
that can be used in conjuction with `docker:dind` service to make most of docker command available to your runner.
This image is based on [`docker:latest`][1] image with a few pre-installed application and custom scripts to help 
**Continuous Integration** pipeline easier 

---

### applications:
- bash 
- git 
- openssh 
- make 
- py-pip
- docker-compose
- sudo
- wget

---

### custom scripts:
- `getgitver.sh`: set GITMAJOR GITMINOR BUILDVER from the processed git commit
- `incgitver.sh`: increase the version variable of git, defaulted to BUILDVER
- `pushtag.sh`: push a version tag according to GITMAJOR.GITMINOR.BUILDVER variable
- `registrypush.sh`: push built docker image to project registry with current version and latest tag

[1]: https://hub.docker.com/_/docker/